# Borg backup

Permet de lancer des backup avec borg. Les cybles peuvent être distantes ou local. Pour la programation des sauvegardes il utilise backupninja

## Installation

### Variables
 * **backup_encryption_pass** => Mot de passe de chiffrement. Il est généré par ansible. Il est nécessaire de contrôler son fonctionnement à postériori. Il peu arriver qu'un caractère interdit soit généré.
* **local_directory** => Cible local pour la sauvegarde. Pratique pour un disque dur externe par exemple. S'il n'est pas setté alors il n'y a pas de sauvegarde locale.
* **backup_host** => Cible distante des sauvegardes. Doit avoir été initialisé avec le role [borg ssh docker](https://framagit.org/cycles-manivelles/ansible-role_borg-ssh-docker)
* **backup_sources**: les répertoires à sauvegarder. Par défaut:
  * /srv
  * /var/log
  * /var/lib/docker/volumes
 * **local_backup_user** (défaut: root) => utilisateur pour les sauvegardes


## Restore

Proposition pour réstaurer une sauvegarde. Mieux vaut se fier à la documentation borg pour l'utiliser au mieux.

``` bash
backup_name=$((export BORG_PASSPHRASE="{{ backup_pass }}" ; borg list --last 1 {{ backup_path }}) | awk '{print $1}')

(export BORG_PASSPHRASE="{{ backup_pass }}" ; borg mount {{ backup_path }}::$backup_name mountpoint)

rsync -arAXv var /
rsync -arAXv srv /
```
